package com.lyj.petmeetingapi.repository;

import com.lyj.petmeetingapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet,Long> {
}
