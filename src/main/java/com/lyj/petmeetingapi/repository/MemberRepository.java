package com.lyj.petmeetingapi.repository;

import com.lyj.petmeetingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Long> {
}
