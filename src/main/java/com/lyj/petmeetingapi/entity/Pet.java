package com.lyj.petmeetingapi.entity;

import com.lyj.petmeetingapi.enums.PetKind;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private PetKind petKind;

    @Column(nullable = false, length = 20)
    private String petName;
}
