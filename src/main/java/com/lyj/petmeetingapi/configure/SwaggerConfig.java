package com.lyj.petmeetingapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "펫미팅 App",
                description = "동물애호가모임",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("동물애호가모임 API v1")
                .pathsToMatch(paths)
                .build();
    }

}
