package com.lyj.petmeetingapi.service;


import com.lyj.petmeetingapi.entity.Member;
import com.lyj.petmeetingapi.model.Member.MemberChangeRequest;
import com.lyj.petmeetingapi.model.Member.MemberCreateRequest;
import com.lyj.petmeetingapi.model.Member.MemberItem;
import com.lyj.petmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();
        for(Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());

            result.add(addItem);

        }
        return result;
    }


    public void putMember(long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();

    }

}
