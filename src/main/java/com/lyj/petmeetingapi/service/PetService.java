package com.lyj.petmeetingapi.service;

import com.lyj.petmeetingapi.entity.Member;
import com.lyj.petmeetingapi.entity.Pet;
import com.lyj.petmeetingapi.model.Pet.PetCreateRequest;
import com.lyj.petmeetingapi.model.Pet.PetItem;
import com.lyj.petmeetingapi.model.Pet.PetResponse;
import com.lyj.petmeetingapi.repository.PetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@AllArgsConstructor

public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member,PetCreateRequest request){
        Pet addData = new Pet();
        addData.setMember(member);
        addData.setPetKind(request.getPetKind());
        addData.setPetName(request.getPetName());

        petRepository.save(addData);
    }

    public List<PetItem> getPets(){
        List<Pet> originList = petRepository.findAll();

        List<PetItem> result = new LinkedList<>();
        for(Pet pet :originList){
            PetItem addItem = new PetItem();
            addItem.setMemberId(pet.getMember().getId());
            addItem.setName(pet.getMember().getName());
            addItem.setPhoneNumber(pet.getMember().getPhoneNumber());
            addItem.setPetKind(pet.getPetKind().getName());
            addItem.setPetName(pet.getPetName());

            result.add(addItem);

        }
        return result;
    }


    public PetResponse getPet(Long id){
        Pet pet = petRepository.findById(id).orElseThrow();
        PetResponse response = new PetResponse();
        response.setId(pet.getId());
        response.setId(pet.getMember().getId());
        response.setName(pet.getMember().getName());
        response.setPhoneNumber(pet.getMember().getPhoneNumber());
        response.setPetKind(pet.getPetKind().getName());
        response.setPetName(pet.getPetName());

        return response;
    }
}
