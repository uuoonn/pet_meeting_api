package com.lyj.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberItem {
    private Long id;
    private String name;
}
