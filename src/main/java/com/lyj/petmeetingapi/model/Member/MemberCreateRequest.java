package com.lyj.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
}
