package com.lyj.petmeetingapi.model.Pet;

import com.lyj.petmeetingapi.enums.PetKind;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PetItem {
    private Long memberId;
    private String name;
    private String phoneNumber;
    private String petKind;
    private String petName;

}
