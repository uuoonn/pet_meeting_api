package com.lyj.petmeetingapi.model.Pet;

import com.lyj.petmeetingapi.enums.PetKind;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PetCreateRequest {
    private PetKind petKind;
    private String petName;
}
