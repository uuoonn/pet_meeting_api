package com.lyj.petmeetingapi.model.Pet;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;

@Getter
@Setter

public class PetResponse {
    private Long id;
    private Long memberId;
    private String name;
    private String phoneNumber;
    private String petKind;
    private String petName;


}
