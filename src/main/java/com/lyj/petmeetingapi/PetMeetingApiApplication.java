package com.lyj.petmeetingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetMeetingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetMeetingApiApplication.class, args);
	}

}
