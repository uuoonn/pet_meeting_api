package com.lyj.petmeetingapi.controller;

import com.lyj.petmeetingapi.entity.Member;
import com.lyj.petmeetingapi.model.Pet.PetCreateRequest;
import com.lyj.petmeetingapi.model.Pet.PetItem;
import com.lyj.petmeetingapi.model.Pet.PetResponse;
import com.lyj.petmeetingapi.service.MemberService;
import com.lyj.petmeetingapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final MemberService memberService;
    private final PetService petService;

    @PostMapping("/pet-creat/{memberId}")
    public String setPet(@PathVariable long memberId, @RequestBody PetCreateRequest request){
        Member member = memberService.getData(memberId);
        petService.setPet(member,request);

        return "펫정보입력완료";
    }

    @GetMapping("/all")
    public List<PetItem> getPets(){
        return petService.getPets();
    }


    @GetMapping("/detail/{id}")
    public PetResponse getPet(@PathVariable long id){
        return petService.getPet(id);
    }
}
