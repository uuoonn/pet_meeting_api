package com.lyj.petmeetingapi.controller;

import com.lyj.petmeetingapi.model.Member.MemberCreateRequest;
import com.lyj.petmeetingapi.model.Member.MemberItem;
import com.lyj.petmeetingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet-meeting")

public class MemberController {

    private final MemberService memberService;

    @PostMapping("/member-creat")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);
        return "회원정보입력완료";

    }


    @GetMapping("/more")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();

    }





}
