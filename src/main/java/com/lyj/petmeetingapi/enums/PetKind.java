package com.lyj.petmeetingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum PetKind {
    DOG("강아지"),
    CAT("고양이"),
    RABBIT("토끼"),
    BIRD("새"),
    HAMSTER("햄스터");

    private String name;
}
